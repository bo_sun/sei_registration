/*********************************************
 * Author: Bo Sun                            *
 * Afflication: TAMS, University of Hamburg  *
 * E-Mail: bosun@informatik.uni-hamburg.de   *
 *         user_mail@QQ.com                  *
 * Date: Nov 13, 2014                        *
 * Licensing: GNU GPL license.               *
 *********************************************/

#ifndef TAMS_SEI_REGISTRATION_H_
#define TAMS_SEI_REGISTRATION_H_

#ifndef TYPE_DEFINITION_
#define TYPE_DEFINITION_
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/visualization/pcl_visualizer.h>
typedef pcl::PointXYZ PointT;
typedef pcl::PointCloud<PointT> PointCloudT;
typedef pcl::visualization::PointCloudColorHandlerCustom<PointT> ColorHandlerT;
typedef Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrixRowXf;
#endif /*TYPE_DEFINITION_*/

std::string object_filename_;
std::string scene_filename_;
int sei_dim = 64; // B of SOFT
int hist_bin = 12;
bool property = 0;
bool debug = 0;
bool resample = 0;
bool tr = 0;
int iteration = 2;
double icp_maxcor = 0.1;
Eigen::Vector3f voxel_size;


/** \brief computeResolution (cloud) compute the mean distance
  * between the points in scan cloud.
  */
double computeResolution (const PointCloudT::ConstPtr &cloud);

/** \brief computeRange (cloud, minpt, maxpt) compute the range
  * of the scan cloud
  */
void computeRange(const PointCloudT::ConstPtr &cloud, PointT &minpt, PointT &maxpt);

/** \brief cart2sph(X,Y,Z, azimuth, polar) transforms Cartesian coordinates stored in
  * corresponding elements of arrays X, Y, and Z into spherical coordinates.
  * azimuth and polar are angular displacements in radians.
  * azimuth(longitudinal) is the counterclockwise angle in the x-y plane
  * measured from the positive x-axis.
  * polar(colatitudianl) is the polar angle measured from the z axis.
  *
  * 0 < azimuth < 2*M_PI; 0 < polar < M_PI
  */
void tams_cart2sph(float x, float y, float z,
					float& azimuth, float& polar);

/** \brief tams_vector_normalization normalize the input vector
  * Parameters:
  * [in]     tams_vector   the input vector
  * [out]    tams_vector   the normalized vector (values are in range [0,1])
  */
void tams_vector_normalization (std::vector<float> &tams_vector);

/** \brief tams_vector2entropy compute the entropy of a vector
  * Parameters:
  * [in]   tams_vector     the input vector
  * [in]   hist_bin        the size of histogram in entropy computation
  * [out]  entropy         the resultant entropy
  */
void tams_vector2entropy( std::vector<float> tams_vector,
                          const size_t hist_bin,
                          float& entropy);

/** \brief computeSEI calculate the SEI of the input point cloud
  * Parameters:
  * [in]     cloud         the input vector
  * [in]     sei_dim       the dimension of SEI(sei_dim X sei_dim)
  * [in]     hist_bin      the size of histogram in entropy computation
  * [out]    entropy       the resulstant SEI stored in a (sei_dim X sei_dim) matrix
  */
void computeSEI( const PointCloudT cloud,
                 size_t sei_dim,
                 size_t hist_bin,
                 EigenMatrixRowXf &entropy);

/** \brief PhaseCorrelation1D compute the offset between two input vectors
  * based on POMF (Phase Only Matched Filter)
  * -->> Q(k) = conjugate(S(k))/|S(k)| * R(k)/|R(k)|
  * -->> q(x) = ifft(Q(k))
  * -->> (xs,ys) = argmax(q(x))
  * Note that the storage order of FFTW is row-order, while the storage
  * order of Eigen is DEFAULT column-order.
  */
void PhaseCorrelation1D(const double *signal,
                        const double *pattern,
                        const int size,
                        int &offset);

/** \brief PhaseCorrelation2D compute the offset between two input images
  * based on POMF (Phase Only Matched Filter)
  * -->> Q(k) = conjugate(S(k))/|S(k)| * R(k)/|R(k)|
  * -->> q(x) = ifft(Q(k))
  * -->> (xs,ys) = argmax(q(x))
  * Note that the storage order of FFTW is row-order, while the storage
  * order of Eigen is default column-order.
  * Parameters:
  * [in] signal              the input(signal) image
  * [in] pattern             the input(pattern) image
  * [in] height              the height of input images(how many rows/size of column/range of x)
  * [in] width               the width of input images (how many columns/size of row/range of y)
  * [out] height_offset      the result offset, we move down (positive x axis) pattern height_offset to match signal
  * [out] width_offset       the result offset, we move right (positive y axis) pattern width_offset to match signal
  */
void PhaseCorrelation2D(const EigenMatrixRowXf signal,
                        const EigenMatrixRowXf pattern,
                        const int height,
                        const int width,
                        int &height_offset,
                        int &width_offset);

/** \brief PhaseCorrelation3D compute the offset between two input volumes
  * based on POMF (Phase Only Matched Filter)
  * -->> Q(k) = conjugate(S(k))/|S(k)| * R(k)/|R(k)|
  * -->> q(x) = ifft(Q(k))
  * -->> (xs,ys) = argmax(q(x))
  * Note that the storage order of FFTW is row-order
  * We adopt the RIGHT-hand Cartesian coordinate system.
  * Parameters:
  * [in] signal              the input(signal) volume
  * [in] pattern             the input(pattern) volume
  * [in] height              the height of input volumes(range of x)
  * [in] width               the width of input volumes (range of y)
  * [in] depth               the depth of input volumes (range of z)
  * [out] height_offset      the result offset, we move down (positive x axis) pattern
  *                          height_offset to match signal
  * [out] width_offset       the result offset, we move right (positive y axis) pattern
  *                          width_offset to match signal
  * [out] depth_offset       the result offset, we move close to viewer (positive z axis) pattern
  *                          depth_offset to match signal
  */
void PhaseCorrelation3D(const double *signal,
                        const double *pattern,
                        const int height,
                        const int width,
                        const int depth,
                        int &height_offset,
                        int &width_offset,
                        int &depth_offset);

/** \brief voxelsize2volumesize compute the size of volume
  * the points cloud rendered based on the size of voxel.
  * Parameters:
  * [in]  cloud         the input point cloud
  * [in]  voxelsize     the size of the voxel
  * [out] volumesize    the size of volume the input cloud should be rendered to
  */
void voxelsize2volumesize ( const PointCloudT cloud,
                            Eigen::Vector3f voxelsize,
                            Eigen::Vector3i &volumesize);

/** \brief point2volume render the input point cloud to a volume
  * Parameters:
  * [in]  cloud              the input point cloud
  * [in]  voxelsize          the size of the voxel
  * [in]  volume_minpt       the minimum coordinates the volume stores
  * [in]  volume_maxpt       the maximum coordinates the volume stores
  * [out] volume_xy          the resultant xy image of the volume stores the maximum z coordinate in pixel xy
  * [out] volume_z	     the resultant z of the volume stores the number of points according to z
  */
void point2volume (const PointCloudT cloud,
                   Eigen::Vector3f voxelsize,
                   Eigen::Vector3f volume_minpt,
                   Eigen::Vector3f volume_maxpt,
                   EigenMatrixRowXf &volume_xy, double *volume_z);
#endif /*TAMS_SEI_REGISTRATION_H_*/
