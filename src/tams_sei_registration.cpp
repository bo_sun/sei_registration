/*********************************************
 * Author: Bo Sun                            *
 * Afflication: TAMS, University of Hamburg  *
 * E-Mail: bosun@informatik.uni-hamburg.de   *
 *         user_mail@QQ.com                  *
 * Date: Nov 13, 2014                        *
 * Licensing: GNU GPL license.               *
 *********************************************/

#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/console/print.h>
#include <pcl/console/parse.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/filter.h>
#include <pcl/registration/icp.h>

#include <vector>
#include <math.h>
#include <algorithm>
#include <eigen3/Eigen/Dense>

#include "tams_soft_fftw_correlate.h"
#include "tams_sei_registration.h"
#include "tams_sei_registration.hpp"

// For debug
#include <cmath>
#include <chrono>
#include <ratio>
#include <fstream>

using namespace std::chrono;

#ifndef TYPE_DEFINITION_
#define TYPE_DEFINITION_
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/visualization/pcl_visualizer.h>
// Types
typedef pcl::PointXYZ PointT;
typedef pcl::PointCloud<PointT> PointCloudT;
typedef pcl::visualization::PointCloudColorHandlerCustom<PointT> ColorHandlerT;
typedef Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrixRowXf;
#endif /*TYPE_DEFINITION_*/

void showhelp(char *filename)
{
    std::cout << std::endl;
    std::cout << "------------------------------------------------------------------------" << std::endl;
    std::cout << "-                                                                      -" << std::endl;
    std::cout << "-               TAMS_SEI_REGISTRATION     User Guide                   -" << std::endl;
    std::cout << "-                                                                      -" << std::endl;
    std::cout << "------------------------------------------------------------------------" << std::endl;
    std::cout << "Usage:" << std::endl;
    std::cout << filename << " object_filename.pcd scene_filename.pcd [Options]" << std::endl;
    std::cout << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << "      -h               Show this help." << std::endl;
    std::cout << "      -property        Show the property of the point clouds." << std::endl;
    std::cout << "      -debug           Debug Model, show some information and save some data." << std::endl;
    std::cout << "      -resample        Resample the original point cloud." << std::endl;
    std::cout << "      -recordtime      Record the time. Please note when record time less output." << std::endl;
    std::cout << "      --icp_maxcor     Set the size of the max correspondence distance in ICP." << std::endl;
    std::cout << "      --iteration      Set the iteration number." << std::endl;
    std::cout << "      --sei_dim        Set the dimension of SEI" << std::endl;
    std::cout << "      --hist_bin       Set the dimension of histogram in entropy computation" << std::endl;
    std::cout << "      --voxel_size     Set the size of voxel when render point cloud into volume" << std::endl;
    std::cout << std::endl;
}

void parseCommandLine(int argc, char **argv)
{
    if (argc < 3)
    {
        showhelp(argv[0]);
        exit(-1);
    }

    if (pcl::console::find_switch(argc, argv, "-h"))
    {
        showhelp(argv[0]);
        exit(0);
    }

    if (pcl::console::find_switch(argc, argv, "-property"))
    {
        property = 1;
    }

    if (pcl::console::find_switch(argc, argv, "-debug"))
    {
        debug = 1;
    }

    if (pcl::console::find_switch(argc, argv, "-resample"))
    {
        resample = 1;
    }

    if (pcl::console::find_switch(argc, argv, "-recordtime"))
    {
        tr = 1;
        debug = 0;
        pcl::console::print_highlight("Please note there are less outputs and hints when record time! \n");
    }

    std::vector<int> filenames;
    filenames = pcl::console::parse_file_extension_argument(argc, argv, ".pcd");
    if (filenames.size()!=2)
    {
        std::cout << "\nFilenames missing.\n";
        showhelp(argv[0]);
        exit(-1);
    }
    object_filename_ = argv[filenames[0]];
    scene_filename_  = argv[filenames[1]];

    pcl::console::parse_argument(argc, argv, "--sei_dim", sei_dim);
    pcl::console::parse_argument(argc, argv, "--hist_bin", hist_bin);
    pcl::console::parse_argument(argc, argv, "--icp_maxcor", icp_maxcor);
    pcl::console::parse_argument(argc, argv, "--iteration", iteration);
    if(pcl::console::parse_3x_arguments(
                argc, argv, "--voxel_size", voxel_size(0), voxel_size(1), voxel_size(2))
            ==-1)
    {
        voxel_size << 0.1, 0.1, 0.1;
    }
}

int
main (int argc, char**argv)
{
    parseCommandLine(argc, argv);

    // Point clouds
    PointCloudT::Ptr object(new PointCloudT);
    PointCloudT::Ptr object_rotated(new PointCloudT);
    PointCloudT::Ptr scene(new PointCloudT);

    // Load object and scene point clouds
    if (pcl::io::loadPCDFile<PointT>(object_filename_, *object)<0 ||
            pcl::io::loadPCDFile<PointT>(scene_filename_,*scene)<0)
    {
        pcl::console::print_error("Error loading object/scene file!\n");
        return (1);
    }

    // Remove the NaN points in object and scene if any
    pcl::console::print_highlight("Remove the NaN points if any...\n");
    std::vector<int> indices_object_nan, indices_scene_nan;
    pcl::removeNaNFromPointCloud(*object,*object,indices_object_nan);
    pcl::removeNaNFromPointCloud(*scene, *scene, indices_scene_nan);

    // compute the resolution of the point clouds if necessary
    if (property)
    {
        double object_resolution = 0.0, scene_resolution = 0.0;
        object_resolution = computeResolution(object);
        scene_resolution = computeResolution(scene);

        PointT object_minpt, object_maxpt;
        PointT scene_minpt, scene_maxpt;
        computeRange(object, object_minpt, object_maxpt);
        computeRange(scene,  scene_minpt,  scene_maxpt);
        pcl::console::print_info("The range of object is [x: %f, y: %f, z: %f]\n",
                                 object_maxpt.x-object_minpt.x,
                                 object_maxpt.y-object_minpt.y,
                                 object_maxpt.z-object_minpt.z);
        pcl::console::print_info("The range of scene is [x: %f, y: %f, z: %f]\n",
                                 scene_maxpt.x-scene_minpt.x,
                                 scene_maxpt.y-scene_minpt.y,
                                 scene_maxpt.z-scene_minpt.z);
        if(resample)
        {
            pcl::console::print_info("The object has %d effective points after resample.\n", object->size());
            pcl::console::print_info("The scene  has %d effective points after resample.\n", scene->size());
            pcl::console::print_info("The resolution of object is %f after resample. \n", object_resolution);
            pcl::console::print_info("The resolution of scene  is %f after resample. \n", scene_resolution);
        }
        else
        {
            pcl::console::print_info("The object has %d effective points without resample.\n", object->size());
            pcl::console::print_info("The scene  has %d effective points without resample.\n", scene->size());
            pcl::console::print_info("The resolution of object is %f without resample. \n", object_resolution);
            pcl::console::print_info("The resolution of scene  is %f without resample. \n", scene_resolution);
        }
        return (0);
    }

    // variables declaration
    int bwIn = sei_dim;
    int bwOut = sei_dim;
    int digLim = sei_dim-1;
    double alpha = 0.0 , beta = 0.0, gamma = 0.0;

    // final transform
    Eigen::Matrix4f final_transform = Eigen::Matrix4f::Zero(4,4);
    final_transform (0,0) = 1.0;
    final_transform (1,1) = 1.0;
    final_transform (2,2) = 1.0;
    final_transform (3,3) = 1.0;

    // scene only compute once
    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    EigenMatrixRowXf sei_scene  = EigenMatrixRowXf::Zero(2*sei_dim, 2*sei_dim);
    Eigen::VectorXf sei_scene_vector  = Eigen::VectorXf::Zero(2*sei_dim*2*sei_dim);
    computeSEI(*scene, sei_dim, hist_bin, sei_scene);
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double> > (t2-t1);
    pcl::console::print_info("Generate SEI of scene uses %f seconds.\n", time_span.count());
    // sei resize (when eigen matrix resize to a vector, its storage order matters!)
    sei_scene.resize(2*sei_dim*2*sei_dim,1);
    sei_scene_vector << sei_scene;

    for (int itr = 0; itr < iteration; itr++)
    {
        pcl::console::print_info("Iteration = %d \n", itr);

        // generate SEI
        pcl::console::print_highlight("Generate the SEIs...\n");
        t1 = high_resolution_clock::now();
        EigenMatrixRowXf sei_object = EigenMatrixRowXf::Zero(2*sei_dim, 2*sei_dim);
        computeSEI(*object, sei_dim, hist_bin, sei_object);
        t2 = high_resolution_clock::now();
        time_span = duration_cast<duration<double> > (t2-t1);
        pcl::console::print_info("Iteration %d---Generate SEI uses %f seconds.\n", itr, time_span.count());

        // sei resize (when eigen matrix resize to a vector, its storage order matters!)
        Eigen::VectorXf sei_object_vector = Eigen::VectorXf::Zero(2*sei_dim*2*sei_dim);
        sei_object.resize(2*sei_dim*2*sei_dim,1);
        sei_object_vector << sei_object;

        if (sei_object_vector.size()!= 2*sei_dim*2*sei_dim ||
                sei_scene_vector.size()!=2*sei_dim*2*sei_dim)
        {
            pcl::console::print_error("SEI resize Failed!\n");
            return (1);
        }
        sei_object.resize(0,0);

        // rotation recovery
        pcl::console::print_highlight("Rotation recovery...\n");
        t1 = high_resolution_clock::now();
        tams_soft_fftw_correlate(sei_scene_vector,
                                 sei_object_vector,
                                 bwIn, bwOut, digLim,
                                 alpha, beta, gamma);
        if (!tr) pcl::console::print_info("Rotation results: alpha = %f, beta = %f, gamma = %f\n", alpha, beta, gamma);
        t2 = high_resolution_clock::now();
        time_span = duration_cast<duration<double> > (t2-t1);
        pcl::console::print_info("Iteration %d---Rotation Estimation uses %f seconds.\n", itr, time_span.count());

        // generate the rotation matrix
        Eigen::Affine3f transform = Eigen::Affine3f::Identity();
        transform.prerotate(Eigen::AngleAxisf(gamma,Eigen::Vector3f::UnitZ()));
        transform.prerotate(Eigen::AngleAxisf(beta,Eigen::Vector3f::UnitY()));
        transform.prerotate(Eigen::AngleAxisf(alpha,Eigen::Vector3f::UnitZ()));

        pcl::transformPointCloud(*object, *object_rotated, transform);

        if(debug)
        {
            // show rotation result
            pcl::visualization::PCLVisualizer visu("Rotation Result");
            visu.setBackgroundColor(255,255,255);
            visu.addPointCloud (object_rotated, ColorHandlerT(object, 0.0, 255.0, 0.0), "object_rotated");
            visu.addPointCloud (scene, ColorHandlerT (scene, 0.0, 0.0, 255.0), "scene");
            visu.spin();
        }

        // estimate the translation
        pcl::console::print_highlight("Estimate the translation...\n");
        t1 = high_resolution_clock::now();

        PointT object_minpt, object_maxpt;
        PointT scene_minpt, scene_maxpt;
        computeRange(object_rotated, object_minpt, object_maxpt);
        computeRange(scene,  scene_minpt,  scene_maxpt);

        // determine the range of the volume
        Eigen::Vector3f volume_minpt;
        Eigen::Vector3f volume_maxpt;
        volume_minpt << std::min(object_minpt.x, scene_minpt.x) - 2.0,
                std::min(object_minpt.y, scene_minpt.y) - 2.0,
                std::min(object_minpt.z, scene_minpt.z) - 2.0;
        volume_maxpt << std::max(object_maxpt.x, scene_maxpt.x) +2.0,
                std::max(object_maxpt.y, scene_maxpt.y) +2.0,
                std::max(object_maxpt.z, scene_maxpt.z) +2.0;

        // determine the size of the volume
        Eigen::Vector3i volumesize;
        volumesize(0) = ceil((volume_maxpt(0)-volume_minpt(0))/voxel_size(0))+1;
        volumesize(1) = ceil((volume_maxpt(1)-volume_minpt(1))/voxel_size(1))+1;
        volumesize(2) = ceil((volume_maxpt(2)-volume_minpt(2))/voxel_size(2))+1;

        if(debug)
        {
            pcl::console::print_info("The volume size: x: %d, y: %d, z: %d. \n", volumesize(0),
                                     volumesize(1), volumesize(2));
        }

        // convert point cloud to volume
        EigenMatrixRowXf object_xy = EigenMatrixRowXf::Zero (volumesize(0), volumesize(1));
        EigenMatrixRowXf scene_xy  = EigenMatrixRowXf::Zero (volumesize(0), volumesize(1));
        double *object_z = (double*) calloc (volumesize(2), sizeof(double));
        double *scene_z  = (double*) calloc (volumesize(2), sizeof(double));

        point2volume(*object_rotated, voxel_size, volume_minpt, volume_maxpt,
                     object_xy, object_z);
        point2volume(*scene, voxel_size, volume_minpt, volume_maxpt,
                     scene_xy, scene_z);

        int offset_x = 0, offset_y = 0, offset_z = 0;
        PhaseCorrelation2D(scene_xy, object_xy,
                           volumesize(0), volumesize(1),
                           offset_x, offset_y);
        PhaseCorrelation1D(scene_z, object_z,
                           volumesize(2), offset_z);
        t2 = high_resolution_clock::now();
        time_span = duration_cast<duration<double> > (t2-t1);
        pcl::console::print_info("Iteration %d---computing translation uses %f seconds.\n", itr, time_span.count());

        transform.translation() << offset_x*voxel_size(0),
                offset_y*voxel_size(1),
                offset_z*voxel_size(2);

        pcl::transformPointCloud(*object, *object, transform);

        if (debug)
        {
            pcl::visualization::PCLVisualizer visu2("SEI Final");
            visu2.setBackgroundColor(255,255,255);
            visu2.addPointCloud(object, ColorHandlerT (object, 0.0,255.0, 0.0), "object_aligned");
            visu2.addPointCloud(scene, ColorHandlerT(scene, 0.0, 0.0, 255.0), "scene");
            visu2.spin();
        }

        if (debug)
        {
            final_transform.block(0,0,3,3) << transform.rotation()*final_transform.block(0,0,3,3);
            final_transform (0,3) = final_transform(0,3) + transform(0,3);
            final_transform (1,3) = final_transform(1,3) + transform(1,3);
            final_transform (2,3) = final_transform(2,3) + transform(2,3);
        }
    }

    // free memory
    sei_scene.resize(0,0);

    // icp refinement
    pcl::console::print_info("Refine the result  using ICP...\n");
    t1 = high_resolution_clock::now();
    pcl::IterativeClosestPoint<PointT, PointT> icp;
    icp.setInputSource(object);
    icp.setInputTarget(scene);
    PointCloudT::Ptr final (new PointCloudT);
    icp.setTransformationEpsilon(1e-06);
    icp.setMaxCorrespondenceDistance(icp_maxcor);
    icp.setMaximumIterations(100);
    icp.align(*final);
    t2 = high_resolution_clock::now();
    time_span = duration_cast<duration<double> > (t2-t1);
    pcl::console::print_info("Refinement based on plain ICP uses %f seconds.\n", time_span.count());

    // save the final result
    if(debug||tr)
        pcl::io::savePCDFileASCII("object_final.pcd", *final);

    std::cout << "ICP has converged: " << icp.hasConverged() << " score: " <<
                 icp.getFitnessScore() << std::endl;
    if (debug)
    {
         Eigen::Matrix4f icp_transformation = icp.getFinalTransformation();
         final_transform.block(0,0,3,3) << icp_transformation.block(0,0,3,3)*final_transform.block(0,0,3,3);
         final_transform (0,3) = final_transform(0,3) + icp_transformation(0,3);
         final_transform (1,3) = final_transform(1,3) + icp_transformation(1,3);
         final_transform (2,3) = final_transform(2,3) + icp_transformation(2,3);

         ofstream file_transformation;
         file_transformation.open("transformation.txt");
         file_transformation << "Transform '";
         file_transformation << object_filename_ << "'" << std::endl;
         file_transformation << "according to the following matrix to match " << std::endl;
         file_transformation << "'" << scene_filename_ << "'" << std::endl;
         file_transformation << std::endl;
         file_transformation << final_transform << std::endl;
         file_transformation.close();

         printf ("Rotation matrix Estimated by SEI-Based registration algorithm & ICP :\n");
         printf ("    | %6.3f %6.3f %6.3f | \n", final_transform (0, 0), final_transform (0, 1), final_transform (0, 2));
         printf ("R = | %6.3f %6.3f %6.3f | \n", final_transform (1, 0), final_transform (1, 1), final_transform (1, 2));
         printf ("    | %6.3f %6.3f %6.3f | \n", final_transform (2, 0), final_transform (2, 1), final_transform (2, 2));
         printf ("Translation vector :\n");
         printf ("t = < %6.3f, %6.3f, %6.3f >\n\n", final_transform (0, 3), final_transform (1, 3), final_transform (2, 3));
    }

    if (debug||tr)
    {
        pcl::visualization::PCLVisualizer visual("Final");
        visual.setBackgroundColor(255,255,255);
        visual.addPointCloud (final, ColorHandlerT (final, 0.0, 255.0, 0.0), "object_final");
        visual.addPointCloud (scene, ColorHandlerT (scene, 0.0, 0.0, 255.0), "scene");
        visual.spin();
    }

    return (0);
}

